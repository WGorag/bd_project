%fonction permettant de réaliser une classification un utilisant le
%classificateur de bayes
function resultat = bayes(X_tr,Y_tr,X_ts,Y_ts, distrib)

    %permet de choisir une distribution et de créer le classificateur
    if exist('distrib','var')
        modele_bayes = fitcnb(X_tr, Y_tr,'DistributionNames', distrib);
    else
        modele_bayes = fitcnb(X_tr, Y_tr);
    end

    %prediction
    test_bayes = modele_bayes.predict(X_ts);
    
    %calcul des éléments permettant d'évaluer les résultats
    erreur = calculErreur(test_bayes, Y_ts);
    recall = rappel(test_bayes, Y_ts);
    preci = precision(test_bayes, Y_ts);
    fScore = FScore(recall, preci);
    resultat = [erreur,  recall, preci,fScore];
end

