# Classification de fausses nouvelles


Vous trouverez dans ce document tous les outils nécessaires à la détection de fausses nouvelles

# Les données

  - Fake.csv(pas présent dans le repo git car trop volumineux)
  - True.csv(pas présent dans le repo git car trop volumineux)
  - ech_false.xls
  - ech_true.xls
  - ech_fake_hot.mat
  - ech_true_hot.mat

# Les fonctions

  - affichage.m
  - bayes.m
  - calculErreur.m
  - comparatif.m
  - FScore.m
  - kppv.m
  - precision.m
  - rappel.m
  - regression.m
  - ReseauNeurone.m
  - svm.m

# Les scripts
  - classificateur_hot_vector.m
  - classificateur_lsa.m
  - echantillonnage_normalisation.m
  - generation_ech_hot_vector.m


# Etape 1
Lancer le script "echantillonnage_normalisation.m" qui générera un échantillon normalisé des fichiers "Fake.csv" et "True.csv". Les fichiers créés lors de cette étape sont "ech_false.xls" et "ech_true.xls".

# Etape 1 bis
Si vous voulez par la suite réaliser une classification sur des hots vectors il faut lancer le script "generation_ech_hot_vector.m" qui générera les hots vectors des fichiers suivants "ech_false.xls" et "ech_true.xls". Les fichiers créés lors de cette étape sont "ech_fake_hot.mat" et "ech_true_hot.mat".

# Etape 2
Lancer les scripts permettant la classification que voici :
  - "classificateur_lsa.m" ce script se base sur les fichiers suivants "ech_false.xls" et "ech_true.xls" auxquels il applique un LSA sur chaque données. Ce script est assez rapide mais n'a pas des résultats très convaincants.
  - "classificateur_hot_vector.m" ce script réalise des classifications sur des données provenant de hot vector. Ce script est plus lent que le premier mais les résultats sont plus pertinants.
 
    
# Nota Bene
Si vous voulez réaliser l'étape 1 ou 1bis vous devrez supprimer au préalable les fichiers que ces scripts générent.
Vous pouvez retrouver les données des fichiers "True.csv" et "Fake.csv" à cette url : https://www.kaggle.com/clmentbisaillon/fake-and-real-news-dataset.

