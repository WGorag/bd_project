%Réseau de neurone
function [resultat,test_neuronne] = ReseauNeurone(modele, X_tr,Y_tr, X_ts, Y_ts)
    %Configuration du réseau de neurone passé en parametre
    modele = configure(modele,X_tr',Y_tr');
    
    %Entrainement du réseau de neuronne
    modele_neuronne = train(modele,X_tr' ,Y_tr');
    
    %Test du réseau de neuronne
    test_neuronne = modele_neuronne(X_ts');
    
    %on associer la prédiction à la  classe prédite
    test_neuronne(test_neuronne >= 0.5) = 1;
    test_neuronne(test_neuronne < 0.5) = 0;
    
    %Evaluation des prédictions
    erreur = calculErreur(test_neuronne, Y_ts);
    recall = rappel(test_neuronne, Y_ts);
    preci = precision(test_neuronne, Y_ts);
    fScore = FScore(recall, preci);
    resultat = [erreur,  recall, preci,fScore];
end

