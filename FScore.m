%calcul le fscore
function result = FScore(recall,preci)
    result = 2 * (recall * preci) / (recall + preci);
end

