%calcul la précision
function precision = precision(tab1, tab2)
    nbVraiFake = 0;
    nbFauxFake = 0;
    for j = 1 : size(tab2,1)
        if( tab1(j)== 0 && tab2(j) == 1)
            nbFauxFake = nbFauxFake + 1; 
        end
        if( (tab1(j) + tab2(j)) == 2)
            nbVraiFake = nbVraiFake + 1; 
        end
    end
    precision = nbVraiFake/(nbVraiFake + nbFauxFake);
end