%parametrage du script
clear all;
clc;
warning('off');

%recuperation des données
true = load("ech_true_hot.mat");
true = cell2mat(struct2cell(true(1)));

fake = load("ech_fake_hot.mat");
fake = cell2mat(struct2cell(fake(1)));

%fusion des fake et vrai nouvelles
combine = [true; fake];
X = combine(1:end, 1:end-1);
Y = combine(1:end, end);
N = size(X,1);

%nombre de validation croisées
n = 5;

% Indice de chaque validation parmis les N données
indices = crossvalind('Kfold',N,n);

%Création du réseau de neurones
reseau_neuronne = feedforwardnet(10);

%initialisation des tableaux contenant l'exactitude, le rappel, la
%precision et le fscore
resultKppv = [];
resultRegression = [];
resultBayes = [];
resultNeurone = [];
resultSvm =[];
for i = 1 : n
    %Je sélectionne mes données d'entrainement
    X_tr = X(indices ~=  i,:);
    Y_tr = Y(indices ~=  i);
    N_tr = size(Y_tr,1);
    
    %Je sélectionne mes données de test
    X_ts = X(indices == i,:);
    Y_ts = Y(indices == i);
    N_ts = size(Y_ts,1);
    
    resultKppv = [resultKppv; kppv(X_tr,Y_tr,X_ts,Y_ts)];
        
    resultSvm = [resultSvm; svm(X_tr,Y_tr,X_ts,Y_ts)];
    
    resultBayes = [resultBayes, bayes(X_tr,Y_tr,X_ts,Y_ts, 'mn')];
    
    resultNeurone = [resultNeurone; ReseauNeurone(reseau_neuronne,X_tr,Y_tr,X_ts,Y_ts)];

    resultRegression = [resultRegression; regression(X_tr,Y_tr,X_ts,Y_ts,"hierarchical")];
    


     fprintf(' %4.2f \n', i/n*100)
     
end
% error_regression = 0
% for i = 1 : n
%     Je sélectionne mes données d'entrainement
%     X_tr = X(indices ~=  i,:);
%     Y_tr = Y(indices ~=  i);
%     N_tr = size(Y_tr,1);
%     
%     Je sélectionne mes données d'entrainement
%     X_ts = X(indices == i,:);
%     Y_ts = Y(indices == i);
%     N_ts = size(Y_ts,1);
%     
%                 
%      
%     reglin = mnrfit(X_tr,categorical(Y_tr));
%     test_regression = X_ts*reglin(2:end)+reglin(1);
%     test_regression(test_regression<0)=1;
%     test_regression(test_regression>0)=0;
%     e_regression = calculErreur(test_regression, Y_ts);
%     error_regression = error_regression + e_regression;
% end
% error_regression / n

%affichage des résultats
affichage("KPPV", resultKppv)
affichage("BAYES", resultBayes);
affichage("SVM", resultSvm);
affichage("RESEAU DE NEURONE", resultNeurone);
affichage("REGRESSION LOGISTIQUE", resultRegression);


