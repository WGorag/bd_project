clear all;
clc;
warning('off');

true = readtable('ech_true.xls');
XTrue = true(1:end, 1:end-1);
YTrue = table2array(true(1:end, end));
fake = readtable('ech_false.xls');

XFake = fake(1:end, 1:end-1);
YFake = table2array(fake(1:end, end));
combine = [true; fake];
X = combine(1:end, 1:end-1);
Y = table2array(combine(1:end, end));
N = size(X,1);

combineLsa = lsaCombine(XFake, XTrue);
XLsa = combineLsa(1:end, 1:end-1);
YLsa = combineLsa(1:end, end);
NLsa = size(XLsa,1);

% %nombre de validation croisées
n = 20;
% 
%Indice de chaque validation parmis les N données
indices = crossvalind('Kfold',NLsa,n);

modele_neurone = feedforwardnet(15);
resultKppv = [];
resultRegression = [];
resultBayes = [];
resultNeurone = [];
resultSvm =[];
resultcomparatif = [];
for i = 1 : n
    %Je sélectionne mes données d'entrainement
    X_tr = XLsa(indices ~=  i,:);
    Y_tr = YLsa(indices ~=  i);
    N_tr = size(Y_tr,1);
    
    %Je sélectionne mes données d'entrainement
    X_ts = XLsa(indices == i,:);
    Y_ts = YLsa(indices == i);
    N_ts = size(Y_ts,1);
    
    resultRegression = [resultRegression; regression(X_tr,Y_tr,X_ts,Y_ts)];
    
    [r_kppv, test_kppv] = kppv(X_tr,Y_tr,X_ts,Y_ts);
    resultKppv = [resultKppv; r_kppv];
    
    [r_neurone, test_neurone] = ReseauNeurone(modele_neurone,X_tr,Y_tr,X_ts,Y_ts);
    resultNeurone = [resultNeurone; ReseauNeurone(modele_neurone,X_tr,Y_tr,X_ts,Y_ts)];
    
    [r_svm, test_svm] = svm(X_tr,Y_tr,X_ts,Y_ts,"gaussian");
    resultSvm = [resultSvm; r_svm];
    
    resultBayes = [resultBayes, bayes(X_tr,Y_tr,X_ts,Y_ts)];
    
    resultcomparatif = [resultcomparatif; comparatif(test_kppv, test_svm, test_neurone, Y_ts)];
    
end

affichage("KPPV", resultKppv)
affichage("REGRESSION LOGISTIQUE", resultRegression);
affichage("BAYES", resultBayes);
affichage("RESEAU DE NEURONE", resultNeurone);
affichage("SVM", resultSvm);
affichage("COMBINAISON (KPPV, SVM, NEURONE)", resultcomparatif);

%Sort un tableau en combinant le LSA des fausses nouvelles et des vraies
function array = lsaCombine(tabFake,tabTrue)
    fakeLsaScore = lsa(tabFake,"r");
    fakeLsaScoreNamed = fakeLsaScore;
    fakeLsaScoreNamed(:,end+1) = 0;
  
    trueLsaScore = lsa(tabTrue,"b");
    
    trueLsaScoreNamed = trueLsaScore;
    trueLsaScoreNamed(:,end+1) = 1;
hold off
    array = [trueLsaScoreNamed; fakeLsaScoreNamed];
end

%fonction permettant le LSA et ecrit les points sur le graph
function array = lsa(tab,color)
    numComponents = 10;
    exposant = 0.5; 
    documents = tokenizedDocument(table2array(tab));
    bag = bagOfWords(documents);
    mdl = fitlsa (bag, numComponents, ... 
        'FeatureStrengthExponent' , exposant);
    array = mdl.DocumentScores;
    for i = 1 : size(array) 
        plot3(array(i,1),array(i,2),array(i,3),'.','Color',color)
        hold on   
    end

end






