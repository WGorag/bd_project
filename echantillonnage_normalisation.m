clear all;
clc;
%%Choix du nombre d'échantillon pour chaque type de news
numberOfDoc = 250;

%%Recuperation des fichiers
%% Ces fichiers ne sont pas de base dans le dossier car il sont
%%trop lourd. Vous trouverez le lien pour les télécharger dans
%%le READ.MD
true = readtable('True.csv');
true(1,:) = [];
indices_true = randi(size(true,1), numberOfDoc,1);
true = true(indices_true,:);
fake = readtable('Fake.csv');
fake(1,:) = [];
indices_fake = randi(size(fake,1), numberOfDoc,1);
fake = fake(indices_fake,:);



for i = 1 : size(fake)
    
    fakeConcate(i,1) = normalize(fake(i, :));
    
    trueConcate(i,1) = normalize(true(i, :)) ;
    
    fprintf(' %4.2f \n', i/numberOfDoc*100)
    
end
%On tag les new en fonction de leur type
trueConcate(:,2) = 1;
fakeConcate(:,2) = 0;

%%on stock les échantillons
xlswrite("ech_true.xls",trueConcate);
xlswrite("ech_false.xls",fakeConcate);

%%Affiche les mots les plus présent dans chaque type de news
figure
subplot(1,2,1)
wFalse = wordcloud(fakeConcate,'Color','red');
subplot(1,2,2)
wTrue = wordcloud(trueConcate,'Color','blue');


%%Cette fonction nous permet de normaliser les articles que nous lui
%%donnons
function document = normalize(article)
    words = [stopWords, 's', 't', 'm', 'o']; %ajout de stop word supplémentaire
    title = getTitle(article);
    text = getText(article);
    document = lower(strcat(title,{' '},text));
    document = tokenizedDocument(document, 'Language','en');
    document = removeShortWords(document,3);
    document = correctSpelling(document);
    document = erasePunctuation(document);
    document = addPartOfSpeechDetails(document);
    document = normalizeWords(document,'Style','lemma');
    document = removeWords(document,words);
    document = joinWords(document(1));
end

%%Cette fonction recupere le titre d'un article
function title = getTitle(article)
    title = string(table2cell(article(1,1)));
end
%%Cette fonction recuperer le texte d'un article
function text = getText(article)
    text = string(table2cell(article(1,2)));
end

