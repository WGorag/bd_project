%calcul le taux d'erreur pour une prédiction
function [erreur] = calculErreur(tab1, tab2)
    nbErreur = 0;
    for j = 1 : size(tab2,1)
      if( tab1(j) ~= tab2(j))
         nbErreur = nbErreur + 1; 
      end
    end
    erreur = nbErreur/size(tab2,1);
end