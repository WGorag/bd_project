%permet de realiser une regression logistique
function [resultat, test_regression] = regression(X_tr,Y_tr,X_ts,Y_ts,model)
   
    %Choix et création du model
    if exist('model','var')
        modele_regression = mnrfit(X_tr, categorical(Y_tr),'model',model);
    else
        modele_regression = mnrfit(X_tr, categorical(Y_tr));
    end  
    
    %je test mon modèle
    test_regression = modele_regression(1) + X_ts * modele_regression(2:end);
   
    test_regression(test_regression >= 0) = 0;
    test_regression(test_regression < 0) = 1;
   
    %on calcul les éléments permettant d'estimer la qualité des résultats
    erreur = calculErreur(test_regression, Y_ts);
    recall = rappel(test_regression, Y_ts);
    preci = precision(test_regression, Y_ts);
    fScore = FScore(recall, preci);
    resultat = [erreur,  recall, preci,fScore];
end

