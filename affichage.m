%fonction permettant l'affichage des résultats
function affichage(name, data)
    taille = size(data,1);
    moyErr = 0;
    moyRappel = 0;
    MoyPreci = 0;
    MoyFScore = 0;
    %on calcul les moyennes des informations avant des les afficher
    for(i = 1 : taille)
        moyErr = moyErr + data(i,1);
        moyRappel = moyRappel + data(i,2);
        MoyPreci = MoyPreci + data(i,3);
        MoyFScore = MoyFScore + data(i,4);
    end
    moyErr = moyErr /taille;
    moyRappel = moyRappel /taille;
    MoyPreci = MoyPreci /taille;
    MoyFScore = MoyFScore /taille;
    
    
    fprintf("[ %s ] \n", name);
    fprintf("==========================\n");
    fprintf("Erreur : %4.2f\n", moyErr);
    fprintf("Rappel : %4.2f\n", moyRappel);
    fprintf("Precision : %4.2f\n", MoyPreci);
    fprintf("F-Score : %4.2f\n", MoyFScore);
    fprintf("==========================\n");
end

