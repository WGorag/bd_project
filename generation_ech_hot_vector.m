clear all;
clc;
warning('off');


%recuperation des echantillon
true = readtable('ech_true.xls');
XTrue = string(table2array(true(1:end, 1:end-1)));
fake = readtable('ech_false.xls');
XFake = string(table2array(fake(1:end, 1:end-1)));


fakeWords = [];
trueWords = [];

%isole chaque mot du document
for i = 1 : size(XFake,1)
    fakeWords = [fakeWords split(XFake(i,1))'];
    trueWords = [trueWords split(XTrue(i,1))'];
end
%création d'un vecteur de mot
comparator = union(trueWords, fakeWords);

%calcul de la probabilité d'apparation de chaque mot dans chaque type de
%news
for i = 1 : size(XFake,1)
    fakeHot =  hotVector(split(XFake(i,1))',comparator);
    trueHot =  hotVector(split(XTrue(i,1))',comparator);
    proba(fakeHot, 0);
    [vector,ratioTrue,ratioFalse] = proba(trueHot,1);
end

%calcul de l'information mutuelle
tab = infoMutuelle(vector,ratioTrue,ratioFalse);

maxValues=n_max_values(tab,300);
mot_restant(comparator, maxValues);
comparator = keep_max_value(comparator, maxValues);
trues = [];
fakes = [];
for i = 1 : size(XFake,1)
    fakeHot =  hotVector(split(XFake(i,1))',comparator);
    if(sum(fakeHot ~= 0))
        fakes = [fakes ; fakeHot];
    end
    trueHot =  hotVector(split(XTrue(i,1))',comparator);
    if(sum(trueHot ~= 0))
        trues = [trues ; trueHot];
    end
end
trues(:,end + 1) = 1;
fakes(:, end + 1) = 0;
save('ech_true_hot.mat','trues');
save('ech_fake_hot.mat','fakes');



function vector = hotVector(text, comparator)
    vector = zeros(1,size(comparator,2));
    for(i = 1 : size(text,2))
        if ismember(text(i), comparator)
            vector(find(comparator == text(i))) = 1;
        end
    end
end

function [arrayProba, ratioTrue, ratioFalse] = proba(vector, isTrue,reset)
    persistent array;
    persistent cptTrue;
    persistent cptFalse;
    if exist('reset','var')
        array = zeros(2,size(vector,2));
        cptTrue = 0;
        cptFalse = 0;
        return
    end
    if isempty(array)
        array = zeros(2,size(vector,2));
        cptTrue = 0;
        cptFalse = 0;
    end
    if isTrue == 1
        cptTrue = cptTrue + 1;
        temp = array(1,:);
        temp = temp + vector;
        temp = temp / cptTrue;
        array(1, :) = temp;
        arrayProba = array;
    else
        cptFalse = cptFalse + 1;
        temp = array(2,:);
        temp = temp + vector;
        temp = temp / cptFalse;
        array(2,:) = temp;
        arrayProba = array;
    end
    
    ratioTrue = (cptTrue / cptFalse)/2;
    ratioFalse =  (cptFalse / cptTrue)/2;
end

function arrayInfoMutuelle = infoMutuelle(vector, ratioTrue, ratioFalse, reset)
    persistent array;
    if exist('reset','var')
        array = zeros(1,size(vector,2));
        return
    end
    if isempty(array)
        array = zeros(1,size(vector,2));
    end
    size(vector,2)
    for(i = 1 : size(vector,2))
        sum = 0;
        
        xy11 = vector(1,i) / ratioTrue;
        xy01 = 0.5 - xy11;
        xy10 = vector(2,i) / ratioFalse;
        xy00 = 0.5 - xy10;
        
        x1 = xy11 + xy10;
        x0 = 1 - x1; 
        y1 = xy01 + xy11;
        y0 = 1 - y1;
        
        if(xy11 ~= 0 && x1 ~= 0 && y1 ~= 0)
            sum = xy11 * log(xy11/(x1*y1));
        end
        if(xy10 ~= 0 && x1 ~= 0 && y0 ~= 0)
            sum = sum + xy10 * log(xy10/(x1*y0));
        end
        if(xy01 ~= 0 && x0 ~= 0 && y1 ~= 0)
            sum = sum + xy01 * log(xy01/(x0*y1));
        end
        if(xy00 ~= 0 && x0 ~= 0 && y0 ~= 0)
            sum = sum + xy00 * log(xy00/(x0*y0));
        end
        array(i) = sum;
        
    end
    arrayInfoMutuelle = array;
end

function [binary_tab] = n_max_values(tab, n)

    max_values = [];
    tab_cpy = tab;
    
    for i=1:1:n
        max_values(i)=max(tab);
        tab(tab == max(tab)) = [];
    end
    binary_tab = double(ismember(tab_cpy,max_values));
end


function array = keep_max_value(vector, max_value_vector)
    vector(find(max_value_vector == 0)) = [];
    array = vector;
end

function mot_restant(vector, max_value_vector)
    figure
    wordcloud(vector(find(max_value_vector == 1))');
end