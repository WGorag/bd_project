%permet de générer un classificateur a partir de 3 autre classificateur
function [resultat] = comparatif(test_1,test_2,test_3,Y_ts)

    %Réalisation de la prédiction
    for i=1:1:length(test_2)
        val=test_1(i)+test_2(i)+test_3(i)/3;
        if (val < 0.5)
            val=0;
        else
            val=1;
        end
        erreur_moyenne(i)=val;
    end
    
    %Evaluation de la prédiction
    erreur = calculErreur(erreur_moyenne, Y_ts);
    recall = rappel(erreur_moyenne, Y_ts);
    preci = precision(erreur_moyenne, Y_ts);
    fScore = FScore(recall, preci);
    resultat = [erreur,  recall, preci,fScore];
end