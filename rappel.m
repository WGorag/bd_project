%calcul le rappel
function rappel = rappel(tab1, tab2)
    nbFake = 0;
    nbVraiFake = 0;
    for j = 1 : size(tab2,1)
        if(tab2(j) == 0)
            nbFake = nbFake + 1;
        end
        if( (tab1(j) + tab2(j)) == 0)
            nbVraiFake = nbVraiFake + 1; 
        end
    end
    rappel = nbVraiFake/nbFake;
end
