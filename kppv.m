%Création du classificateur kppv
function [resultat, test_kppv] = kppv(X_tr,Y_tr, X_ts, Y_ts)
    %Création du kppv
    modele_kppv = fitcknn(X_tr, Y_tr,'NumNeighbors',5);
    %prédiction
    test_kppv = predict(modele_kppv,X_ts);
    %Evaluation des prédiction
    erreur = calculErreur(test_kppv, Y_ts);
    recall = rappel(test_kppv, Y_ts);
    preci = precision(test_kppv, Y_ts);
    fScore = FScore(recall, preci);
    resultat = [erreur,  recall, preci,fScore];
end

