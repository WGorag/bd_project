%création du classificateur svm
function [resultat, test_svm]  = svm(X_tr,Y_tr, X_ts, Y_ts, kernel)
    %Création du classificateur et choix du kernel
    if exist('kernel','var')
        class_svm = fitcsvm(X_tr, Y_tr, 'KernelFunction',kernel,...
        'KernelScale','auto');
    else
        class_svm = fitcsvm(X_tr, Y_tr);
    end
    %Prédiction
    test_svm = class_svm.predict(X_ts);
    
    %Evaluation des résultats
    erreur = calculErreur(test_svm, Y_ts);
    recall = rappel(test_svm, Y_ts);
    preci = precision(test_svm, Y_ts);
    fScore = FScore(recall, preci);
    resultat = [erreur,  recall, preci,fScore];
end

